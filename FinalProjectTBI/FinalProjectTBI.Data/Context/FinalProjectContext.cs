﻿using FinalProjectTBI.Data.Configurations;
using FinalProjectTBI.Data.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace FinalProjectTBI.Data
{
    public class FinalProjectContext : IdentityDbContext<ApplicationUser>
    {
        public FinalProjectContext(DbContextOptions<FinalProjectContext> options)
            : base(options)
        {
        }

        public FinalProjectContext()
        {
        }

        public DbSet<ReceivedEmail> Emails { get; set; }
        public DbSet<EmailAttachment> EmailAttachments { get; set; }
        public DbSet<EmailBody> EmailBodies { get; set; }
        public DbSet<ApplicationUser> EmailManagerUsers { get; set; }
        public DbSet<LoanApplicant> LoanApplicants { get; set; }
        public DbSet<LoanApplication> LoanApplications { get; set; }
        public DbSet<LoanDecision> LoanDecisions { get; set; }
        public DbSet<EmailStatus> EmailStatuses { get; set; }
        public DbSet<Mediator> Mediators { get; set; }
        public DbSet<GmailLoginCredential> GmailLoginCredentials { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new EmailAttachmentConfiguration());
            builder.ApplyConfiguration(new EmailBodyConfiguration());
            builder.ApplyConfiguration(new LoanApplicantConfiguration());
            builder.ApplyConfiguration(new LoanApplicationConfiguration());
            builder.ApplyConfiguration(new LoanDecisionConfiguration());
            builder.ApplyConfiguration(new EmailStatusConfiguration());
            builder.ApplyConfiguration(new MediatorConfiguration());
            builder.ApplyConfiguration(new ReceivedEmailConfiguration());

            base.OnModelCreating(builder);
        }

        public static void SeedUserAdmin(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<FinalProjectContext>();

                if (dbContext.Roles.Any(u => u.Name == "Manager"))
                {
                    return;
                }

                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                roleManager.CreateAsync(new IdentityRole { Name = "Manager" }).Wait();
                roleManager.CreateAsync(new IdentityRole { Name = "Operator" }).Wait();

                var adminUser = new ApplicationUser { UserName = "manager@manager.com", Email = "manager@manager.com" };
                userManager.CreateAsync(adminUser, "#TBIManager2019").Wait();

                userManager.AddToRoleAsync(adminUser, "Manager").Wait();
            }
        }
    }
}
