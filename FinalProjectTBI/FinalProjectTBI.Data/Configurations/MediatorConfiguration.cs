﻿using FinalProjectTBI.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalProjectTBI.Data.Configurations
{
    public class MediatorConfiguration : IEntityTypeConfiguration<Mediator>
    {
        public void Configure(EntityTypeBuilder<Mediator> builder)
        {
            builder.HasKey(m => m.Id);

            builder.Property(m => m.Name)
                .IsRequired();

            builder.Property(m => m.PersonalEmail)
                .IsRequired();
        }
    }
}
