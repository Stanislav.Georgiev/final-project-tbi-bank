﻿using FinalProjectTBI.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalProjectTBI.Data.Configurations
{
    public class EmailBodyConfiguration : IEntityTypeConfiguration<EmailBody>
    {
        public void Configure(EntityTypeBuilder<EmailBody> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Content)
                .IsRequired();
        }
    }
}
