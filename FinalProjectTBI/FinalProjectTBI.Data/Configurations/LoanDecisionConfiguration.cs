﻿using FinalProjectTBI.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalProjectTBI.Data.Configurations
{
    public class LoanDecisionConfiguration : IEntityTypeConfiguration<LoanDecision>
    {
        public void Configure(EntityTypeBuilder<LoanDecision> builder)
        {
            builder.HasKey(ld => ld.Id);

            builder.Property(ld => ld.Decision)
                .IsRequired();
        }
    }
}
