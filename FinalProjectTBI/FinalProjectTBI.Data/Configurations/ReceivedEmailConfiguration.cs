﻿using FinalProjectTBI.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalProjectTBI.Data.Configurations
{
    public class ReceivedEmailConfiguration : IEntityTypeConfiguration<ReceivedEmail>
    {
        public void Configure(EntityTypeBuilder<ReceivedEmail> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Subject);

            builder.Property(e => e.CreatedOn)
                .IsRequired();

            builder
              .HasOne(e => e.EmailBody)
              .WithOne(eb => eb.ReceivedEmail)
              .HasForeignKey<ReceivedEmail>(e => e.BodyId);

            builder
              .HasOne(e => e.EmailStatus)
              .WithMany(es => es.ReceivedEmails)
              .HasForeignKey(e => e.EmailStatusId);

            builder
              .HasOne(e => e.Mediator)
              .WithMany(m => m.Emails)
              .HasForeignKey(e => e.MediatorId);
        }
    }
}
