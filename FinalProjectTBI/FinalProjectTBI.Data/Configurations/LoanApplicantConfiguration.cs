﻿using FinalProjectTBI.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalProjectTBI.Data.Configurations
{
    public class LoanApplicantConfiguration : IEntityTypeConfiguration<LoanApplicant>
    {
        public void Configure(EntityTypeBuilder<LoanApplicant> builder)
        {
            builder.HasKey(la => la.Id);

            builder.Property(la => la.Name)
               .IsRequired();

            builder.Property(la => la.ClientNumber)
              .IsRequired();
        }
    }
}
