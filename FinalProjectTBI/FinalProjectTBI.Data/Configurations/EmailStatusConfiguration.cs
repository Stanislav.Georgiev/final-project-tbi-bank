﻿using FinalProjectTBI.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinalProjectTBI.Data.Configurations
{
    public class EmailStatusConfiguration : IEntityTypeConfiguration<EmailStatus>
    {
        public void Configure(EntityTypeBuilder<EmailStatus> builder)
        {
            builder.HasKey(ms => ms.Id);

            builder.Property(md => md.Type)
                .IsRequired();
        }
    }
}
