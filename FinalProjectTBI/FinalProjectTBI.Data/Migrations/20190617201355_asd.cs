﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FinalProjectTBI.Data.Migrations
{
    public partial class asd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmailStatusType",
                table: "Emails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EmailStatusType",
                table: "Emails",
                nullable: true);
        }
    }
}
