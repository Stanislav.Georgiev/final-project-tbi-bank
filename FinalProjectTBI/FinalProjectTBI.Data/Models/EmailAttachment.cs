﻿using FinalProjectTBI.Data.Models.Abstracts;

namespace FinalProjectTBI.Data.Models
{
    public class EmailAttachment
    {
        public int Id { get; set; }
        public int? Size { get; set; }
        public string FileName { get; set; }

        public int? EmailId { get; set; }
        public ReceivedEmail Email { get; set; }
    }
}
