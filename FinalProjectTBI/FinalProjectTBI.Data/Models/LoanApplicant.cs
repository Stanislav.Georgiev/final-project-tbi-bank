﻿using FinalProjectTBI.Data.Models.Abstracts;
using System.Collections.Generic;

namespace FinalProjectTBI.Data.Models
{
    public class LoanApplicant : DataModel
    {
        public LoanApplicant()
        {
            this.LoanApplications = new HashSet<LoanApplication>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ClientNumber { get; set; }
        public string PhoneNumber { get; set; }

        public ICollection<LoanApplication> LoanApplications { get; set; }
    }
}