﻿using FinalProjectTBI.Data.Models.Abstracts;
using System.Collections.Generic;

namespace FinalProjectTBI.Data.Models
{
    public class LoanDecision : DataModel
    {
        public LoanDecision()
        {
            this.LoanApplications = new HashSet<LoanApplication>();
        }

        public int Id { get; set; }
        public string Decision { get; set; }

        public ICollection<LoanApplication> LoanApplications { get; set; }
    }
}
