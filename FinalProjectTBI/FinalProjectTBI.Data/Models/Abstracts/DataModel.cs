﻿using FinalProjectTBI.Data.Models.Constracts;
using System;
using System.ComponentModel.DataAnnotations;

namespace FinalProjectTBI.Data.Models.Abstracts
{
    public abstract class DataModel : IAuditable, IDeletable
    {    
        [DataType(DataType.DateTime)]
        public DateTime CreatedOn { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime ModifiedOn { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DeletedOn { get; set; }

        public bool IsDeleted { get; set; }
    }
}
