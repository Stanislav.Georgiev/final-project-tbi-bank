﻿using FinalProjectTBI.Data.Models.Abstracts;
using System.Collections.Generic;

namespace FinalProjectTBI.Data.Models
{
    public class ReceivedEmail : DataModel
    {
        public ReceivedEmail()
        {
            this.EmailAttachments = new HashSet<EmailAttachment>();
        }

        public int Id { get; set; }
        public string EmailId { get; set; }
        public string Subject { get; set; }
        public long Timestamp { get; set; }

        public int? BodyId { get; set; }
        public EmailBody EmailBody { get; set; }

        public int? LoanApplicationId { get; set; }
        public LoanApplication LoanApplication { get; set; }

        public int? MediatorId { get; set; }
        public Mediator Mediator { get; set; }

        public int? EmailStatusId { get; set; }
        public EmailStatus EmailStatus { get; set; }

        public ApplicationUser User { get; set; }

        public ICollection<EmailAttachment> EmailAttachments { get; set; }
    }
}
