﻿using FinalProjectTBI.Data.Models.Abstracts;
using System.Collections.Generic;

namespace FinalProjectTBI.Data.Models
{
    public class EmailStatus : DataModel
    {
        public EmailStatus()
        {
            this.ReceivedEmails = new HashSet<ReceivedEmail>();
        }

        public int Id { get; set; }
        public string Type { get; set; }

        public ICollection<ReceivedEmail> ReceivedEmails { get; set; }
    }
}
