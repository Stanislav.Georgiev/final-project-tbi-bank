﻿using FinalProjectTBI.Data.Models.Abstracts;
using System.Collections.Generic;

namespace FinalProjectTBI.Data.Models
{
    public class Mediator : DataModel
    {
        public Mediator()
        {
            this.Emails = new HashSet<ReceivedEmail>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string PersonalEmail { get; set; }

        public ICollection<ReceivedEmail> Emails { get; set; }
    }
}
