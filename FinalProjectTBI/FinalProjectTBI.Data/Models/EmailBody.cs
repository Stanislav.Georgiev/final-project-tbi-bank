﻿using FinalProjectTBI.Data.Models.Abstracts;

namespace FinalProjectTBI.Data.Models
{
    public class EmailBody
    {
        public int Id { get; set; }
        public string Content { get; set; }

        public ReceivedEmail ReceivedEmail { get; set; } 
    }
}
