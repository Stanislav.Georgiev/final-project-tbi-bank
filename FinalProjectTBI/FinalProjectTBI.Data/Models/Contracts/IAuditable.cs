﻿using System;

namespace FinalProjectTBI.Data.Models.Constracts
{
    public interface IAuditable
    {
        DateTime CreatedOn { get; set; }

        DateTime ModifiedOn { get; set; }
    }
}
