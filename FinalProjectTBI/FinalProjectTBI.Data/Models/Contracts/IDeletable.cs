﻿using System;

namespace FinalProjectTBI.Data.Models.Constracts
{
    public interface IDeletable
    {
        bool IsDeleted { get; set; }

        DateTime DeletedOn { get; set; }
    }
}
