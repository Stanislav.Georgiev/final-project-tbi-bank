﻿using FinalProjectTBI.Data.Models.Abstracts;

namespace FinalProjectTBI.Data.Models
{
    public class LoanApplication : DataModel
    {
        public int Id { get; set; }

        public ApplicationUser User { get; set; }

        public int? ApplicantId { get; set; }
        public LoanApplicant LoanApplicant { get; set; }

        public int? LoanDecisionId { get; set; }
        public LoanDecision LoanDecision { get; set; }
    }
}
