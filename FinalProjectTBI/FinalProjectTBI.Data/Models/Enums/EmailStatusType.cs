﻿using System.ComponentModel.DataAnnotations;

namespace FinalProjectTBI.Services.Enums
{
    public enum EmailStatusType
    {
        [Display(Name="Not Reviewed")]
        NotReviewed = 1,
        [Display(Name="Not Valid")]
        NotValid = 2,
        [Display(Name = "New")]
        New = 3,
        [Display(Name = "Open")]
        Open = 4,
        [Display(Name = "Closed")]
        Closed = 5
    }
}
