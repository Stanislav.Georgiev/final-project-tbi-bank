﻿using System.ComponentModel.DataAnnotations;

namespace FinalProjectTBI.Services.Enums
{
    public enum LoanDecisionType
    {
        [Display(Name = "Approved")]
        Approved = 1,
        [Display(Name = "Rejected")]
        Rejected = 2
    }
}
