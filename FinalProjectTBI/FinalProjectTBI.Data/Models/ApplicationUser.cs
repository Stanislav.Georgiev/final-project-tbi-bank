﻿using FinalProjectTBI.Data.Models.Constracts;
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;


namespace FinalProjectTBI.Data.Models
{
    public class ApplicationUser : IdentityUser, IAuditable, IDeletable
    {
        public ApplicationUser()
        {
            this.CreatedOn = DateTime.Now;
            this.ChangePassword = false;
        }

        public bool ChangePassword { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime CreatedOn { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime ModifiedOn { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DeletedOn { get; set; }

        public bool IsDeleted { get; set; }
    }
}
