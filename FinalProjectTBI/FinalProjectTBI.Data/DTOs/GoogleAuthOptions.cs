﻿namespace FinalProjectTBI.Data.DTOs
{
    public class GoogleAuthOptions
    {
        public string ConsentUrl { get; set; }
        public string TokenUrl { get; set; }
        public string RedirectUrl { get; set; }

        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public string[] Scopes { get; set; }
    }
}
