﻿namespace FinalProjectTBI.Data.DTOs
{
    public class GmailApiUrlOptions
    {
        public string Base { get; set; }
        public string Messages { get; set; }
        public string Message { get; set; }
    }
}
