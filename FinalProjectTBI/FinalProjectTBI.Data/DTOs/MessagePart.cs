﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace FinalProjectTBI.Data.DTOs
{
    public class MessagePart
    {
        [JsonProperty("parts")]
        public IList<MessagePart> Parts { get; set; }

        [JsonProperty("body")]
        public MessagePartBody Body { get; set; }

        [JsonProperty("fileName")]
        public string FileName { get; set; }
    }
}
