﻿using Newtonsoft.Json;

namespace FinalProjectTBI.Data.DTOs
{
    public class MessagePartBody
    {
        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }
    }
}
