﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace FinalProjectTBI.Data.DTOs
{
    public class GmailMessagePayload
    {
        [JsonProperty("headers")]
        public IList<MessagePartHeader> Headers { get; set; }

        [JsonProperty("parts")]
        public IList<MessagePart> Parts { get; set; }

        [JsonProperty("body")]
        public MessagePartBody Body { get; set; }
    }
}
