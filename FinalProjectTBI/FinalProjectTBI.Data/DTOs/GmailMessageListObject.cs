﻿using Newtonsoft.Json;

namespace FinalProjectTBI.Data.DTOs
{
    public class GmailMessageListObject
    {
        [JsonProperty("messages")]
        public GmailMessageObject[] Messages { get; set; }

        [JsonProperty("resultSizeEstimate")]
        public int SizeEstimate { get; set; }

        [JsonProperty("nextPageToken")]
        public string NextPageToken { get; set; }
    }
}
