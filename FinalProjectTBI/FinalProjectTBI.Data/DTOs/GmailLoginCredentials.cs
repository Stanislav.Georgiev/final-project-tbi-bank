﻿using Newtonsoft.Json;

namespace FinalProjectTBI.Data.DTOs
{
    public class GmailLoginCredentials
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("expires_in")]
        public string ExpiresInSec { get; set; }
    }
}
