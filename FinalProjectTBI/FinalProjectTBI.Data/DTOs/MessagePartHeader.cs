﻿using Newtonsoft.Json;

namespace FinalProjectTBI.Data.DTOs
{
    public class MessagePartHeader
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
