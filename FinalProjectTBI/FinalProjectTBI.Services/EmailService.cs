﻿using FinalProjectTBI.Data;
using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using FinalProjectTBI.Services.Enums;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services
{
    public class EmailService : IEmailService
    {
        private readonly FinalProjectContext context;
        private readonly IMediatorService mediatorService;
        private readonly IEmailAttachmentService emailAttachmentService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public EmailService(
            FinalProjectContext context,
            IMediatorService mediatorService,
            IEmailAttachmentService emailAttachmentService,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager
            )
        {
            this.context = context;
            this.mediatorService = mediatorService;
            this.emailAttachmentService = emailAttachmentService;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task SaveEmailsToDbAsync()
        {
            // If modifying these scopes, delete your previously saved credentials
            // at ~/.credentials/gmail-dotnet-quickstart.json
            string[] Scopes = { GmailService.Scope.GmailReadonly };

            UserCredential credential;

            using (var stream =
                new FileStream(@"C:\Users\sdgeo\Desktop\Final Project TBI\FinalProjectTBI\FinalProjectTBI\client_secret.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true));
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = this.GetType().ToString(),
            });

            //GET LABELS
            UsersResource.LabelsResource.ListRequest labelsRequest = service.Users.Labels.List("me");

            IList<Label> labels = labelsRequest.Execute().Labels;
            Console.WriteLine("Labels:");
            if (labels != null && labels.Count > 0)
            {
                foreach (var labelItem in labels)
                {
                    Console.WriteLine("{0}", labelItem.Name);
                }
            }
            else
            {
                Console.WriteLine("No labels found.");
            }
            Console.WriteLine();

            Console.WriteLine("Messages:");

            //GET EMAIL PARTS
            UsersResource.MessagesResource.ListRequest emailListRequest = service.Users.Messages.List("me");

            var lastEmailAdded = await this.context.Emails.LastOrDefaultAsync();

            //DOES NOT WORK!!!
            //if (lastEmailAdded != null)
            //{
            //    var lastEmailTimestamp = lastEmailAdded.Timestamp;
            //    emailListRequest.Q = $"newer: {lastEmailTimestamp.ToString()}";
            //}

            //IT WORKS!!!
            //var date = "2019/05/27"; 
            //emailListRequest.Q = $"after: {date}"; 
            //}

            //if (lastEmailReceivedOn != null)
            //{
            //    request2.Q = "is: unread"; //only get unread;
            //Example: after:2004/04/16

            //emailListRequest.LabelIds = "INBOX";
            //emailListRequest.IncludeSpamTrash = false;


            var emailListResponse = await emailListRequest.ExecuteAsync();

            if (emailListResponse == null || emailListResponse.Messages == null)
            {
                return;
            }

            //foreach (var message in emailListResponse.Messages)
            for (int i = emailListResponse.Messages.Count - 1; i >= 0; i--)
            {
                var emailInfoRequest = service.Users.Messages.Get("me", emailListResponse.Messages[i].Id);

                var emailInfoResponse = await emailInfoRequest.ExecuteAsync();

                if (emailInfoResponse == null)
                {
                    return;
                }

                var timestamp = (long)emailInfoResponse.InternalDate;
                var createdOn = DateTimeOffset.FromUnixTimeMilliseconds(timestamp).DateTime.ToLocalTime();

                var EmailId = emailInfoResponse.Id;

                //EMAIL HEADERS
                string from = "";
                string dateString = "";
                string subject = "";
                string body = "";
                string senderName = "";
                string senderEmail = "";

                IList<MessagePartHeader> parts = emailInfoResponse.Payload?.Headers;

                foreach (var part in parts)
                {
                    if (part.Name == "Date")
                    {
                        dateString = part.Value;
                    }
                    if (part.Name == "From")
                    {
                        from = part.Value;
                        var senderEmailMatch = System.Text.RegularExpressions.Regex.Match(from, "<(.+)>");
                        senderName = from.Substring(0, senderEmailMatch.Index);
                        senderEmail = senderEmailMatch.Groups[1].Value;
                    }
                    else if (part.Name == "Subject")
                    {
                        subject = part.Value;
                    }

                    if (dateString != "" && from != "")
                    {
                        if (dateString != "" && from != "")
                        {
                            if (emailInfoResponse.Payload.Parts == null && emailInfoResponse.Payload.Body != null)
                                body = DecodeBase64String(emailInfoResponse.Payload.Body.Data);
                            else
                                body = GetNestedBodyParts(emailInfoResponse.Payload.Parts, "");
                        }
                    }
                }

                Console.WriteLine($"Id: {EmailId}");
                Console.WriteLine($"Timestamp: {timestamp}");
                Console.WriteLine($"Date: {createdOn}");
                Console.WriteLine($"From: {from}");
                Console.WriteLine($"Subject: {subject}");
                Console.WriteLine($"Body: {body}");
                Console.WriteLine($"Sender name: {senderName}");
                Console.WriteLine($"Sender email: {senderEmail}");


                var email = new ReceivedEmail()
                {
                    EmailId = EmailId,
                    Timestamp = timestamp,
                    CreatedOn = createdOn,
                    Subject = subject,
                };

                await this.context.Emails.AddAsync(email);

                //ATTACHEMENTS
                IList<MessagePart> attachments = emailInfoResponse.Payload?.Parts;

                if (attachments != null)
                {
                    foreach (MessagePart attachment in attachments)
                    {
                        if (!String.IsNullOrEmpty(attachment.Filename))
                        {
                            var emailAttachment = await this.emailAttachmentService.AddEmailAttachmentAsync(attachment.Filename, attachment.Body.Size);
                            emailAttachment.Email = email;

                            Console.WriteLine($"Attachment file name: {attachment.Filename}");
                            Console.WriteLine($"File size: {attachment.Body.Size}");
                        }
                    }
                }

                //TO REMOVE
                var mediator = await this.context.Mediators.FirstOrDefaultAsync(m => m.Name == senderName);

                if (mediator == null)
                {
                    mediator = await this.mediatorService.AddMediatorAsync(senderName, senderEmail);
                }

                await this.context.SaveChangesAsync();
            }
        }

        private static string DecodeBase64String(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return "<strong>Message body was not returned</strong>";

            string InputStr = input.Replace("-", "+").Replace("_", "/");
            return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(InputStr));
        }

        private static string GetNestedBodyParts(IList<MessagePart> part, string curr)
        {
            string str = curr;
            if (part == null)
            {
                return str;
            }
            else
            {
                foreach (var parts in part)
                {
                    if (parts.Parts == null)
                    {
                        if (parts.Body != null && parts.Body.Data != null)
                        {
                            var ts = DecodeBase64String(parts.Body.Data);
                            str += ts;
                        }
                    }
                    else
                    {
                        return GetNestedBodyParts(parts.Parts, str);
                    }
                }

                return str;
            }
        }

        public async Task<IList<ReceivedEmail>> ListAllEmails(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var emails = await this.context.Emails
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Mediator)
                     .Include(e => e.EmailStatus)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.CreatedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();
                return emails;
            }
            else
            {
                var emails = await this.context.Emails
                    .Include(e => e.EmailBody)
                    .Include(e => e.EmailAttachments)
                    .Include(e => e.Mediator)
                    .Include(e => e.EmailStatus)
                    .OrderByDescending(a => a.CreatedOn)
                    .Skip(skip)
                    .Take(pageSize)
                    .ToListAsync();

                return emails;
            }
        }

        public async Task<IList<ReceivedEmail>> ListNewEmails(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == (EmailStatusType.New).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Mediator)
                     .Include(e => e.EmailStatus)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.CreatedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;


            }
            else
            {
                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == (EmailStatusType.New).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Mediator)
                     .Include(e => e.EmailStatus)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.CreatedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;
            }
        }

        public async Task<IList<ReceivedEmail>> ListOpenEmails(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == (EmailStatusType.Open).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Mediator)
                     .Include(e => e.EmailStatus)
                     .Include(e => e.User)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.CreatedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;

            }
            else
            {
                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == (EmailStatusType.Open).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Mediator)
                     .Include(e => e.EmailStatus)
                     .Include(e => e.User)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.CreatedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;
            }
        }

        public async Task<IList<ReceivedEmail>> ListClosedEmails(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == (EmailStatusType.Closed).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Mediator)
                     .Include(e => e.EmailStatus)
                     .Include(e => e.User)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.CreatedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;

            }
            else
            {
                var emails = await this.context.Emails
                     .Where(e => e.EmailStatus.Type == (EmailStatusType.Closed).ToString())
                     .Include(e => e.EmailBody)
                     .Include(e => e.EmailAttachments)
                     .Include(e => e.Mediator)
                     .Include(e => e.EmailStatus)
                     .Include(e => e.User)
                     .Where(a => a.EmailStatus.Type.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.CreatedOn)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;
            }
        }

        public int GetAllEmailsCount()
        {
            var count = this.context.Emails.Count();
            return count;
        }

        public int GetNewEmailsCount()
        {
            var count = this.context.Emails.Where(e => e.EmailStatus.Type == (EmailStatusType.New).ToString()).Count();
            return count;
        }

        public int GetOpenEmailsCount()
        {
            var count = this.context.Emails.Where(e => e.EmailStatus.Type == (EmailStatusType.Open).ToString()).Count();
            return count;
        }

        public int GetClosedEmailsCount()
        {
            var count = this.context.Emails.Where(e => e.EmailStatus.Type == (EmailStatusType.Closed).ToString()).Count();
            return count;
        }

        public ReceivedEmail GetEmail(int id)
        {
            var email = this.context.Emails
                .Include(e => e.EmailBody)
                .Include(e => e.Mediator)
                .Include(e => e.EmailAttachments)
                .Include(e => e.EmailStatus)
                .Where(e => e.Id == id).FirstOrDefault();
            return email;
        }

        public async Task<string> GetRole(ApplicationUser currentUser)
        {
            var roleId = (await this.context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == currentUser.Id)).RoleId;

            var roleName = (await this.context.Roles.FirstOrDefaultAsync(r => r.Id == roleId)).Name;

            return roleName;
        }

        public async Task<ReceivedEmail> ChangeEmailStatusAsync(ReceivedEmail email, string targetEmailStatus, ApplicationUser currentUser)
        {
            var newEmailStatus = await this.context.EmailStatuses
                .FirstOrDefaultAsync(es => es.Type.ToLower() == targetEmailStatus.ToLower());

            if (newEmailStatus == null)
            {
                newEmailStatus = new EmailStatus()
                {
                    Type = targetEmailStatus
                };
                await this.context.EmailStatuses.AddAsync(newEmailStatus);
            }

            email.EmailStatus = newEmailStatus;
            email.ModifiedOn = DateTime.Now;
            email.User = currentUser;

            await this.context.SaveChangesAsync();        

            return email;
        }
    }
}
