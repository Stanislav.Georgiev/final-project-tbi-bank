﻿using FinalProjectTBI.Data;
using FinalProjectTBI.Data.DTOs;
using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using FinalProjectTBI.Services.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services
{
    public class GmailApiService : IGmailApiService
    {
        private readonly IHttpClientFactory factory;
        private readonly GmailApiUrlOptions gmailUrlOptions;
        private readonly IGmailAuthService gmailAuthService;
        private readonly FinalProjectContext context;
        private readonly IMediatorService mediatorService;
        private readonly IEmailAttachmentService emailAttachmentService;

        public GmailApiService(IHttpClientFactory factory, IOptions<GmailApiUrlOptions> gmailOptions, IGmailAuthService gmailAuthService,
            FinalProjectContext context, IMediatorService mediatorService, IEmailAttachmentService emailAttachmentService)
        {
            this.factory = factory ?? throw new ArgumentNullException(nameof(factory));
            this.gmailUrlOptions = gmailOptions?.Value ?? throw new ArgumentNullException(nameof(gmailOptions));
            this.gmailAuthService = gmailAuthService ?? throw new ArgumentNullException(nameof(gmailAuthService));
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mediatorService = mediatorService ?? throw new ArgumentNullException(nameof(mediatorService));
            this.emailAttachmentService = emailAttachmentService ?? throw new ArgumentNullException(nameof(emailAttachmentService));
        }

        public async Task SaveEmailsToDbAsync(string accessToken)
        {
            var newMessagesList = new List<GmailMessageObject>();

            var messageList = await this.SendRequestAsync<GmailMessageListObject>(this.gmailUrlOptions.Messages, accessToken);

            var lastAddedMessageMailboxId = (await this.context.Emails.LastOrDefaultAsync())?.EmailId;

            foreach (var message in messageList.Messages)
            {
                if (message.Id == lastAddedMessageMailboxId)
                {
                    break;
                }

                newMessagesList.Add(message);
            }

            for (int i = newMessagesList.Count - 1; i >= 0; i--)
            {
                var EmailId = newMessagesList[i].Id;

                var message = await this.SendRequestAsync<GmailMessageObject>(this.gmailUrlOptions.Message + EmailId, accessToken);

                string from = "";
                string dateHeader = "";
                string subject = "";
                string body = "";
                string senderName = "";
                string senderEmail = "";

                IList<MessagePartHeader> parts = message.Payload?.Headers;

                foreach (var part in parts)
                {
                    if (part.Name == "From")
                    {
                        from = part.Value;

                        var senderEmailMatch = System.Text.RegularExpressions.Regex.Match(from, "<(.+)>");
                        senderName = from.Substring(0, senderEmailMatch.Index);
                        senderEmail = senderEmailMatch.Groups[1].Value;
                    }
                    else if (part.Name == "Date")
                    {
                        dateHeader = part.Value;
                    }
                    else if (part.Name == "Subject")
                    {
                        subject = part.Value;
                    }

                    if (dateHeader != "" && from != "")
                    {
                        if (message.Payload.Parts == null && message.Payload.Body != null)
                        {
                            //body = DecodeBase64String(message.Payload.Body.Data);

                            body = DecodeBase64String(message.Payload.Body.Data);
                            var result = body.IndexOf('<');
                            body = body.Substring(0, result);
                        }
                        else
                        {
                            //body = GetNestedBodyParts(message.Payload.Parts, "");

                            body = GetNestedBodyParts(message.Payload.Parts, "");
                            var result = body.IndexOf('<');
                            body = body.Substring(0, result);
                        }
                    }
                }

                var mediator = await this.context.Mediators.FirstOrDefaultAsync(m => m.Name == senderName);

                if (mediator == null)
                {
                    mediator = await this.mediatorService.AddMediatorAsync(senderName, senderEmail);
                }

                var timestamp = message.InternalDate;
                var createdOn = DateTimeOffset.FromUnixTimeMilliseconds(timestamp).DateTime.ToLocalTime();

                var emailBody = new EmailBody()
                {
                    Content = body,
                };

                await this.context.EmailBodies.AddAsync(emailBody);

                var emailStatus = await this.context.EmailStatuses.FirstOrDefaultAsync(es => es.Type == (EmailStatusType.NotReviewed).ToString());

                if (emailStatus == null)
                {
                    emailStatus = new EmailStatus()
                    {
                        Type = (EmailStatusType.NotReviewed).ToString()
                    };

                    await this.context.EmailStatuses.AddAsync(emailStatus);
                }

                var email = new ReceivedEmail()
                {
                    EmailId = EmailId,
                    Timestamp = timestamp,
                    CreatedOn = createdOn,
                    Subject = subject,
                    Mediator = mediator,
                    EmailBody = emailBody,
                    EmailStatus = emailStatus,
                    ModifiedOn = DateTime.Now
                };

                await this.context.Emails.AddAsync(email);

                IList<MessagePart> attachments = message.Payload?.Parts;

                if (attachments != null)
                {
                    foreach (var attachment in attachments)
                    {
                        if (!String.IsNullOrEmpty(attachment.FileName))
                        {
                            var emailAttachment = await this.emailAttachmentService.AddEmailAttachmentAsync(attachment.FileName, attachment.Body.Size);
                            emailAttachment.Email = email;
                        }
                    }
                }
                await this.context.SaveChangesAsync();
            }
        }
           
        private async Task<TObject> SendRequestAsync<TObject>(string url, string accessToken)
        {
            var client = this.factory.CreateClient();

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);

            var response = await client.GetAsync(this.gmailUrlOptions.Base + url);

            var result = JsonConvert.DeserializeObject<TObject>(await response.Content.ReadAsStringAsync());

            return result;
        }

        private static string DecodeBase64String(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return "<strong>Message body was not returned</strong>";

            string InputStr = input.Replace("-", "+").Replace("_", "/");
            return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(InputStr));
        }

        private static string GetNestedBodyParts(IList<MessagePart> part, string curr)
        {
            string str = curr;
            if (part == null)
            {
                return str;
            }
            else
            {
                foreach (var parts in part)
                {
                    if (parts.Parts == null)
                    {
                        if (parts.Body != null && parts.Body.Data != null)
                        {
                            var ts = DecodeBase64String(parts.Body.Data);
                            str += ts;
                        }
                    }
                    else
                    {
                        return GetNestedBodyParts(parts.Parts, str);
                    }
                }
                return str;
            }
        }
    }
}
