﻿using FinalProjectTBI.Data;
using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services
{
    public class MediatorService : IMediatorService
    {
        private readonly FinalProjectContext context;

        public MediatorService(FinalProjectContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Mediator> AddMediatorAsync(string name, string mediatorEmail)
        {
            var mediator = await this.context.Mediators.FirstOrDefaultAsync(m => m.Name == name);

            if (mediator != null)
            {
                throw new ArgumentException();
            }

            mediator = new Mediator()
            {
                Name = name,
                PersonalEmail = mediatorEmail
            };

            await this.context.Mediators.AddAsync(mediator);
            await this.context.SaveChangesAsync();

            return mediator;
        }
    }
}
