﻿using FinalProjectTBI.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services
{
    public class GmailHostedService : IHostedService
    {
        private readonly IServiceProvider serviceProvider;
        private Timer timer;

        public GmailHostedService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            this.timer = new Timer(GetNewEmailsFromGmail, null, TimeSpan.Zero,
                TimeSpan.FromMinutes(15));

            return Task.CompletedTask;
        }

        private async void GetNewEmailsFromGmail(object state)
        {
            using (var scope = this.serviceProvider.CreateScope())
            {
                var gmailAuthService = scope.ServiceProvider.GetRequiredService<IGmailAuthService>();

                var credentials = await gmailAuthService.CheckForTokensAsync();

                if (credentials == null)
                {
                    return;
                }
                else
                {
                    var accessToken = await gmailAuthService.GetAccessToken(credentials);

                    var gmailApiService = scope.ServiceProvider.GetRequiredService<IGmailApiService>();

                    //await gmailApiService.SaveEmailsToDbAsync(accessToken);
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
