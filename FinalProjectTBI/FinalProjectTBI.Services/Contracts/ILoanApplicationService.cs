﻿using FinalProjectTBI.Data.Models;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services.Contracts
{
    public interface ILoanApplicationService
    {
        Task<LoanApplication> AddLoanApplicationAsync(LoanApplicant loanApplicant, int emailId);
    }
}
