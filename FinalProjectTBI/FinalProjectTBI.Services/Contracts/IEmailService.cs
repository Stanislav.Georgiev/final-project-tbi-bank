﻿using FinalProjectTBI.Data.DTOs;
using FinalProjectTBI.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services.Contracts
{
    public interface IEmailService
    {   
        Task SaveEmailsToDbAsync();
        Task<IList<ReceivedEmail>> ListAllEmails(int skip, int pageSize, string searchValue);
        Task<IList<ReceivedEmail>> ListNewEmails(int skip, int pageSize, string searchValue);
        Task<IList<ReceivedEmail>> ListOpenEmails(int skip, int pageSize, string searchValue);
        Task<IList<ReceivedEmail>> ListClosedEmails(int skip, int pageSize, string searchValue);
        int GetAllEmailsCount();
        int GetNewEmailsCount();
        int GetOpenEmailsCount();
        int GetClosedEmailsCount();
        ReceivedEmail GetEmail(int id);
        Task<string> GetRole(ApplicationUser currentUser);
        Task<ReceivedEmail> ChangeEmailStatusAsync(ReceivedEmail email, string targetEmailStatus, ApplicationUser currentUser);
    }
}
