﻿using FinalProjectTBI.Data.Models;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services.Contracts
{
    public interface IEmailAttachmentService
    {
        Task<EmailAttachment> AddEmailAttachmentAsync(string fileName, int? size);
    }
}
