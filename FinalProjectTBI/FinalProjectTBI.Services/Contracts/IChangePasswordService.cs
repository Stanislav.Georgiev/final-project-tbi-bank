﻿using FinalProjectTBI.Data.Models;

namespace FinalProjectTBI.Services.Contracts
{
    public interface IChangePasswordService
    {
        void ChangePasswordConfirm(ApplicationUser user);
    }
}
