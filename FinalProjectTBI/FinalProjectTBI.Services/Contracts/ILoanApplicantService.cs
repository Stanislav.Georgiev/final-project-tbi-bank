﻿using FinalProjectTBI.Data.Models;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services.Contracts
{
    public interface ILoanApplicantService
    {
        Task<LoanApplicant> AddLoanApplicantAsync(string name, string personalNumber, string phoneNumber);
    }
}
