﻿using FinalProjectTBI.Data.Models;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services.Contracts
{
    public interface IMediatorService
    {
        Task<Mediator> AddMediatorAsync(string name, string mediatorEmail);
    }
}
