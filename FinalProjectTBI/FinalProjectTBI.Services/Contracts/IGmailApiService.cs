﻿using System.Threading.Tasks;

namespace FinalProjectTBI.Services.Contracts
{
    public interface IGmailApiService
    {
        Task SaveEmailsToDbAsync(string accessToken);
    }
}
