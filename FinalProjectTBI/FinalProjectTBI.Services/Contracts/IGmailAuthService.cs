﻿using FinalProjectTBI.Data.Models;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services.Contracts
{
    public interface IGmailAuthService
    {
        Task<string> GetAccessToken(GmailLoginCredential entityCredentials);
        Task<GmailLoginCredential> CheckForTokensAsync();
        string BuildConsentUrl();
        Task<string> AcquireCredentialsAsync(string authorizationCode);
        Task<string> RefreshAccessTokenAsync(GmailLoginCredential credentials);
    }
}
