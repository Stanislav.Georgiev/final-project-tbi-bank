﻿using FinalProjectTBI.Data;
using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using System;

namespace FinalProjectTBI.Services
{
    public class ChangePasswordService : IChangePasswordService
    {
        private readonly FinalProjectContext context;


        public ChangePasswordService(FinalProjectContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async void ChangePasswordConfirm(ApplicationUser user)
        {
            user.ChangePassword = true;
            await this.context.SaveChangesAsync();
        }
    }
}
