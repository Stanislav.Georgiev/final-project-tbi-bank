﻿using FinalProjectTBI.Data;
using FinalProjectTBI.Data.DTOs;
using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services
{
    public class GmailAuthService : IGmailAuthService
    {
        private const int EXPIRATION_MINUTES = 5;

        private readonly IHttpClientFactory factory;
        private readonly GoogleAuthOptions options;
        private readonly FinalProjectContext context;

        public GmailAuthService(IHttpClientFactory factory, IOptions<GoogleAuthOptions> googleAuthOptions,
            FinalProjectContext context)
        {
            this.factory = factory ?? throw new ArgumentNullException(nameof(factory));
            this.options = googleAuthOptions?.Value ?? throw new ArgumentNullException(nameof(googleAuthOptions));
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<string> GetAccessToken(GmailLoginCredential entityCredentials)
        {
            if (DateTime.Now >= entityCredentials.ExpiryDate.AddMinutes(-EXPIRATION_MINUTES))
            {
                var accessToken = await RefreshAccessTokenAsync(entityCredentials);

                return accessToken;
            }
            else
            {
                return entityCredentials.AccessToken;
            }
        }

        public async Task<string> RefreshAccessTokenAsync(GmailLoginCredential credentials)
        {
            var response = await this.factory
                       .CreateClient(credentials.RefreshToken)
                       .PostAsync(this.options.TokenUrl, CreateRefreshTokenRequestBody(credentials.RefreshToken));

            if (response.IsSuccessStatusCode)
            {
                var deserializedData = JsonConvert.DeserializeObject<GoogleAccessToken>(await response.Content.ReadAsStringAsync());

                SaveCredentials(deserializedData, credentials);

                return deserializedData.AccessToken;
            }
            else
            {
                throw new ArgumentException("Bad response 2!");
            }
        }

        public FormUrlEncodedContent CreateRefreshTokenRequestBody(string refreshToken)
            => new FormUrlEncodedContent(new[]
            {
                        new KeyValuePair<string, string>("client_id", this.options.ClientId),
                        new KeyValuePair<string, string>("client_secret", this.options.ClientSecret),
                        new KeyValuePair<string, string>("refresh_token", refreshToken),
                        new KeyValuePair<string, string>("grant_type", "refresh_token"),
            });

        public async Task<string> AcquireCredentialsAsync(string authorizationCode)
        {
            var response = await this.factory
                .CreateClient()
                .PostAsync(this.options.TokenUrl, CreateAccessTokenRequestBody(authorizationCode));

            if (response.IsSuccessStatusCode)
            {
                var deserializedData = JsonConvert.DeserializeObject<GoogleAccessToken>(await response.Content.ReadAsStringAsync());

                SaveCredentials(deserializedData, null);

                return deserializedData.AccessToken;
            }
            else
            {
                //return Json(await response.Content.ReadAsStringAsync());
                throw new ArgumentException("Bad response 1!");
            }
        }

        private FormUrlEncodedContent CreateAccessTokenRequestBody(string authorizationCode)
            => new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("code", authorizationCode),
                new KeyValuePair<string, string>("client_id", this.options.ClientId),
                new KeyValuePair<string, string>("client_secret", this.options.ClientSecret),
                new KeyValuePair<string, string>("redirect_uri", this.options.RedirectUrl),
                new KeyValuePair<string, string>("grant_type", "authorization_code"),
            });

        public void SaveCredentials(GoogleAccessToken tokens, GmailLoginCredential credentials)
        {
            if (tokens.RefreshToken == null)
            {
                credentials.AccessToken = tokens.AccessToken;
                credentials.ExpiryDate = DateTime.Now.AddSeconds(tokens.ExpireIn);
            }
            else
            {
                this.context.GmailLoginCredentials.Add(new GmailLoginCredential
                {
                    AccessToken = tokens.AccessToken,
                    RefreshToken = tokens.RefreshToken,
                    ExpiryDate = DateTime.Now.AddSeconds(tokens.ExpireIn),
                });
            }
            this.context.SaveChanges();
        }

        public string BuildConsentUrl()
            => new StringBuilder()
            .Append($"{this.options.ConsentUrl}?")
            .Append($"scope= {string.Join(",", this.options.Scopes)}")
            .Append($"&access_type=offline")
            .Append("&include_granted_scopes=true")
            .Append("&response_type=code")
            .Append($"&redirect_uri={this.options.RedirectUrl}")
            .Append($"&client_id={this.options.ClientId}")
            .ToString();

        public async Task<GmailLoginCredential> CheckForTokensAsync()
        {
            var credentials = await this.context.GmailLoginCredentials
                .FirstOrDefaultAsync();

            return credentials;
        }
    }
}
