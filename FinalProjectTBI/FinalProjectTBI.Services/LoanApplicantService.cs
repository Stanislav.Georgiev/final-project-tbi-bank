﻿using FinalProjectTBI.Data;
using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services
{
    public class LoanApplicantService : ILoanApplicantService
    {
        private readonly FinalProjectContext context;

        public LoanApplicantService(FinalProjectContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<LoanApplicant> AddLoanApplicantAsync(string name, string clientNumber, string phoneNumber)

        {
            var loanApplicant = await this.context.LoanApplicants
                .FirstOrDefaultAsync(m => m.ClientNumber == clientNumber);

            if (loanApplicant != null)
            {
                return loanApplicant;
            }

            loanApplicant = new LoanApplicant()
            {
                Name = name,
                ClientNumber = clientNumber,
                PhoneNumber = phoneNumber
            };

            await this.context.LoanApplicants.AddAsync(loanApplicant);
            await this.context.SaveChangesAsync();

            return loanApplicant;
        }
    }
}
