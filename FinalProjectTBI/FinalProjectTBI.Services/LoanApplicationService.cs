﻿using FinalProjectTBI.Data;
using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using FinalProjectTBI.Services.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services
{
    public class LoanApplicationService : ILoanApplicationService
    {
        private readonly FinalProjectContext context;

        public LoanApplicationService(FinalProjectContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<LoanApplication> AddLoanApplicationAsync(LoanApplicant loanApplicant, int emailId)
        {
            var email = await this.context.Emails.FirstOrDefaultAsync(e => e.Id == emailId);

            var loanApplication = new LoanApplication()
            {
                LoanApplicant = loanApplicant
            };

            email.LoanApplication = loanApplication;

            var emailStatus = await this.context.EmailStatuses.FirstOrDefaultAsync(es => es.Type == (EmailStatusType.Open).ToString());

            if (emailStatus == null)
            {
                emailStatus = new EmailStatus()
                {
                    Type = (EmailStatusType.Open).ToString()
                };

                await this.context.EmailStatuses.AddAsync(emailStatus);
            }

            email.EmailStatus = emailStatus;

            await this.context.LoanApplications.AddAsync(loanApplication);
            await this.context.SaveChangesAsync();

            return loanApplication;
        }
    }
}
