﻿using FinalProjectTBI.Data;
using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace FinalProjectTBI.Services
{
    public class EmailAttachmentService : IEmailAttachmentService
    {
        private readonly FinalProjectContext context;

        public EmailAttachmentService(FinalProjectContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<EmailAttachment> AddEmailAttachmentAsync(string fileName, int? size)
        {
            var attachment = new EmailAttachment()
            {
                FileName = fileName,
                Size = size
            };

            await this.context.EmailAttachments.AddAsync(attachment);
            await this.context.SaveChangesAsync();

            return attachment;
        }

    }
}
