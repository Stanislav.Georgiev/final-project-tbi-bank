﻿using System.Threading.Tasks;

namespace FinalProjectTBI.Web.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
