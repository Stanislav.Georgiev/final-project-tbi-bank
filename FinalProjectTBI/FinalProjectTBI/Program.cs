﻿using FinalProjectTBI.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace FinalProjectTBI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            FinalProjectContext.SeedUserAdmin(host);

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
