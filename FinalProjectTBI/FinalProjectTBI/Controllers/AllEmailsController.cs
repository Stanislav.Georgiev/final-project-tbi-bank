﻿using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using FinalProjectTBI.Web.Models;
using FinalProjectTBI.Web.Providers;
using FinalProjectTBI.Web.Models.EmailViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FinalProjectTBI.Services.Enums;
using Microsoft.AspNetCore.Identity;

namespace FinalProjectTBI.Web.Controllers
{
    public class AllEmailsController : Controller
    {
        private readonly IEmailService emailService;
        private readonly IViewModelMapper<ReceivedEmail, EmailViewModel> emailMapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public AllEmailsController(
            IEmailService emailService,
            IViewModelMapper<ReceivedEmail, EmailViewModel> emailMapper,
            UserManager<ApplicationUser> userManager
            )
        {
            this.emailService = emailService;
            this.emailMapper = emailMapper;
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ListAllEmails()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecords = this.emailService.GetAllEmailsCount();
            var emails = await this.emailService.ListAllEmails(skip, pageSize, searchValue);

            var model = emails.Select(x => this.emailMapper.MapFrom(x)).ToList();
            
            var resultJson = Json(new
            {
                draw = draw,
                recordsFiltered = totalRecords,
                recordsTotal = totalRecords,
                data = model,
                MaxJsonLength = Int32.MaxValue,
            });

            resultJson.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            };

            return resultJson;           
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var email = this.emailService.GetEmail(id);
            var model = this.emailMapper.MapFrom(email);
            return PartialView("_DetailsPartial", model);
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);

        [HttpPost]
        public async Task<IActionResult> ChangeEmailToNewStatusAsync(int emailId)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = this.emailService.GetEmail(emailId);
            var currentEmailStatus = email.EmailStatus.Type.ToString();
            var userRole = await this.emailService.GetRole(currentUser);

            try
            {
                var check = ChangeStatusPermissionProvider.GetChangeStatusPermissions(currentEmailStatus, userRole);
                var result = check.Contains((EmailStatusType.New).ToString());
                if (result)
                {
                    var emailResult = await emailService.ChangeEmailStatusAsync(email, (EmailStatusType.New).ToString(), currentUser);
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }

            return Ok("GJ");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeEmailToOpenStatusAsync(int emailId)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = this.emailService.GetEmail(emailId);
            var currentEmailStatus = email.EmailStatus.Type.ToString();
            var userRole = await this.emailService.GetRole(currentUser);

            try
            {
                var check = ChangeStatusPermissionProvider.GetChangeStatusPermissions(currentEmailStatus, userRole);
                var result = check.Contains((EmailStatusType.Open).ToString());
                if (result)
                {
                    var emailResult = await emailService.ChangeEmailStatusAsync(email, (EmailStatusType.Open).ToString(), currentUser);
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }

            return Ok("GJ");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeEmailToClosedStatusAsync(int emailId)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = this.emailService.GetEmail(emailId);
            var currentEmailStatus = email.EmailStatus.Type.ToString();
            var userRole = await this.emailService.GetRole(currentUser);

            try
            {
                var check = ChangeStatusPermissionProvider.GetChangeStatusPermissions(currentEmailStatus, userRole);
                var result = check.Contains((EmailStatusType.Closed).ToString());
                if (result)
                {
                    var emailResult = await emailService.ChangeEmailStatusAsync(email, (EmailStatusType.Closed).ToString(), currentUser);
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }

            return Ok("GJ");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeEmailToNotReviewedStatusAsync(int emailId)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = this.emailService.GetEmail(emailId);
            var currentEmailStatus = email.EmailStatus.Type.ToString();
            var userRole = await this.emailService.GetRole(currentUser);

            try
            {
                var check = ChangeStatusPermissionProvider.GetChangeStatusPermissions(currentEmailStatus, userRole);
                var result = check.Contains((EmailStatusType.NotReviewed).ToString());
                if (result)
                {
                    var emailResult = await emailService.ChangeEmailStatusAsync(email, (EmailStatusType.NotReviewed).ToString(), currentUser);
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }

            return Ok("GJ");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeEmailToNotValidStatusAsync(int emailId)
        {
            var currentUser = await GetCurrentUserAsync();
            var email = this.emailService.GetEmail(emailId);
            var currentEmailStatus = email.EmailStatus.Type.ToString();
            var userRole = await this.emailService.GetRole(currentUser);

            try
            {
                var check = ChangeStatusPermissionProvider.GetChangeStatusPermissions(currentEmailStatus, userRole);
                var result = check.Contains((EmailStatusType.NotValid).ToString());
                if (result)
                {
                    var emailResult = await emailService.ChangeEmailStatusAsync(email, (EmailStatusType.NotValid).ToString(), currentUser);
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }

            return Ok("GJ");
        }

        [HttpGet("404")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
