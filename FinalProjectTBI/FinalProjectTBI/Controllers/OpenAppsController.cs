﻿using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using FinalProjectTBI.Web.Models;
using FinalProjectTBI.Web.Models.EmailViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace FinalProjectTBI.Web.Controllers
{
    public class OpenAppsController : Controller
    {
        private readonly ILoanApplicationService loanApplicationService;
        private readonly ILoanApplicantService loanApplicantService;
        private readonly IEmailService emailService;
        private readonly IViewModelMapper<ReceivedEmail, EmailViewModel> emailMapper;

        public OpenAppsController(
            ILoanApplicationService loanApplicationService, 
            ILoanApplicantService loanApplicantService,
            IEmailService emailService,
            IViewModelMapper<ReceivedEmail, EmailViewModel> emailMapper
            )
        {
            this.loanApplicationService = loanApplicationService ?? throw new ArgumentNullException(nameof(loanApplicationService));
            this.loanApplicantService = loanApplicantService ?? throw new ArgumentNullException(nameof(loanApplicantService));
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            this.emailMapper = emailMapper ?? throw new ArgumentNullException(nameof(emailMapper));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ListOpenEmails()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecords = this.emailService.GetOpenEmailsCount();
            var emails = await this.emailService.ListOpenEmails(skip, pageSize, searchValue);

            var model = emails.Select(x => this.emailMapper.MapFrom(x)).ToList();

            var resultJson = Json(new
            {
                draw = draw,
                recordsFiltered = totalRecords,
                recordsTotal = totalRecords,
                data = model,
                MaxJsonLength = Int32.MaxValue,
            });

            resultJson.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            };

            return resultJson;
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var email = this.emailService.GetEmail(id);
            var model = this.emailMapper.MapFrom(email);
            return PartialView("_DetailsPartial", model);
        }

        [HttpGet("404")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
