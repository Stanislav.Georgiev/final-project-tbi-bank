﻿using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services.Contracts;
using FinalProjectTBI.Web.Models.EmailViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FinalProjectTBI.Web.Controllers
{
    public class NewAppsController : Controller
    {
        private readonly ILoanApplicationService loanApplicationService;
        private readonly ILoanApplicantService loanApplicantService;
        private readonly IViewModelMapper<LoanApplication, LoanApplicationViewModel> loanApplicationMapper;
        private readonly IViewModelMapper<ReceivedEmail, EmailViewModel> emailMapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailService emailService;

        public NewAppsController(
            ILoanApplicationService loanApplicationService, 
            ILoanApplicantService loanApplicantService,
            IViewModelMapper<LoanApplication, LoanApplicationViewModel> loanApplicationMapper,
            UserManager<ApplicationUser> userManager, IViewModelMapper<ReceivedEmail, EmailViewModel> emailMapper,
            IEmailService emailService)
        {
            this.loanApplicationService = loanApplicationService ?? throw new ArgumentNullException(nameof(loanApplicationService));
            this.loanApplicantService = loanApplicantService ?? throw new ArgumentNullException(nameof(loanApplicantService));
            this.loanApplicationMapper = loanApplicationMapper ?? throw new ArgumentNullException(nameof(loanApplicationMapper));
            this.emailMapper = emailMapper ?? throw new ArgumentNullException(nameof(emailMapper));
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            _userManager = userManager;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult Details(int id)
        {
            var email = this.emailService.GetEmail(id);
            var emailBodyContent = email.EmailBody.Content;
            var model = new NewLoanApplicationViewModel();
            model.Email = this.emailMapper.MapFrom(email);	

            var emailViewModel = this.emailMapper.MapFrom(email);
            var content = emailViewModel.EmailBodyContent;
            model.Email = emailViewModel;

            return PartialView("_NewLoanApplicationPartial", model);
        }

        [HttpPost]
        public IActionResult Details(NewLoanApplicationViewModel model)
        {
            var email = this.emailService.GetEmail(model.Email.Id);
            var loanApplication = email.LoanApplication;
            return PartialView("_NewLoanApplicationPartial", model);
        }


        public IActionResult AddNewLoanApplication()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddNewLoanApplication(LoanApplicationViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var loanApplicant = await this.loanApplicantService.
                AddLoanApplicantAsync(
                model.LoanApplicant.Name, model.LoanApplicant.ClientNumber,
                model.LoanApplicant.PhoneNumber);

            var loanApplication = await this.loanApplicationService.
                AddLoanApplicationAsync(loanApplicant, model.Email.Id);

            return RedirectToAction("Index", "Home");
        }    

        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);

        [HttpPost]
        public async Task<IActionResult> ListNewEmails()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecord = this.emailService.GetNewEmailsCount();
            var emails = await this.emailService.ListNewEmails(skip, pageSize, searchValue);


            var model = emails.Select(x => this.emailMapper.MapFrom(x)).ToList();

            var json = Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });

            json.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            };

            return json;
        }
    }
}
