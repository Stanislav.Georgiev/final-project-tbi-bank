﻿using FinalProjectTBI.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FinalProjectTBI.Web.Controllers
{
    public class GmailController : Controller
    {
        private readonly IGmailApiService gmailApiService;
        private readonly IGmailAuthService gmailAuthService;

        public GmailController(IGmailApiService gmailApiService, IGmailAuthService gmailAuthService)
        {
            this.gmailApiService = gmailApiService ?? throw new ArgumentNullException(nameof(gmailApiService));
            this.gmailAuthService = gmailAuthService ?? throw new ArgumentNullException(nameof(gmailAuthService));
        }

        public async Task<IActionResult> StartProgram()
        {
            var credentials = await gmailAuthService.CheckForTokensAsync();

            if (credentials == null)
            {
                return RedirectToAction("GoogleLogin", "Gmail");
            }
            else
            {
                var accessToken = await gmailAuthService.GetAccessToken(credentials);

                await gmailApiService.SaveEmailsToDbAsync(accessToken);

                return RedirectToAction("Login", "Account");
            }
        }

        public IActionResult GoogleLogin()
        {
            var sb = gmailAuthService.BuildConsentUrl();

            return Redirect(sb.ToString());
        }

        [Route("google-callback")]
        public async Task<IActionResult> GoogleCallback(string code)
        {
            var accessToken = await gmailAuthService.AcquireCredentialsAsync(code);

            await gmailApiService.SaveEmailsToDbAsync(accessToken);

            return RedirectToAction("Login", "Account");
        }
    }
}
