﻿using FinalProjectTBI.Data;
using FinalProjectTBI.Data.DTOs;
using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Services;
using FinalProjectTBI.Services.Contracts;
using FinalProjectTBI.Web.Mappers;
using FinalProjectTBI.Web.Models.EmailViewModels;
using FinalProjectTBI.Web.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace FinalProjectTBI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<FinalProjectContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                    .AddEntityFrameworkStores<FinalProjectContext>()
                    .AddDefaultTokenProviders();

            services.AddTransient<IEmailSender, EmailSender>();

            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IMediatorService, MediatorService>();
            services.AddScoped<IEmailAttachmentService, EmailAttachmentService>();
            services.AddScoped<ILoanApplicationService, LoanApplicationService>();
            services.AddScoped<ILoanApplicantService, LoanApplicantService>();
            services.AddScoped<IChangePasswordService, ChangePasswordService>();

            services.AddHostedService<GmailHostedService>();

            services.AddHttpClient();

            services.AddScoped<IGmailAuthService, GmailAuthService>();
            services.AddScoped<IGmailApiService, GmailApiService>();

            services.AddScoped<IViewModelMapper<ReceivedEmail, EmailViewModel>, EmailViewModelMapper>();
            services.AddScoped<IViewModelMapper<EmailAttachment, EmailAttachmentViewModel>, EmailAttachmentViewModelMapper>();
            services.AddScoped<IViewModelMapper<IReadOnlyCollection<ReceivedEmail>, EmailsViewModel>, EmailsViewModelMapper>();
            services.AddScoped<IViewModelMapper<LoanApplicant, LoanApplicantViewModel>, LoanApplicantViewModelMapper>();
            services.AddScoped<IViewModelMapper<LoanApplication, LoanApplicationViewModel>, LoanApplicationViewModelMapper>();


            services.Configure<GmailApiUrlOptions>(this.Configuration.GetSection("GmailApiUrlOptions"));
            services.Configure<GoogleAuthOptions>(this.Configuration.GetSection("GoogleAuthOptions"));

            services.AddResponseCaching();

            services.AddMvc().AddJsonOptions
                (options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);        
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();      
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Gmail}/{action=StartProgram}/{id?}");
            });
        }        
    }
}
