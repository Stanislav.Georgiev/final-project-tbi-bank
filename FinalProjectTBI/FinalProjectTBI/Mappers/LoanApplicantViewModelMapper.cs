﻿using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Web.Models.EmailViewModels;

namespace FinalProjectTBI.Web.Mappers
{
    public class LoanApplicantViewModelMapper : IViewModelMapper<LoanApplicant, LoanApplicantViewModel>
    {
        public LoanApplicantViewModel MapFrom(LoanApplicant entity)
        {
            var loanApplicantViewModel = new LoanApplicantViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                ClientNumber = entity.ClientNumber,
                PhoneNumber = entity.PhoneNumber,
                LoanApplications = entity.LoanApplications
            };

            return loanApplicantViewModel;
        }
    }
}
