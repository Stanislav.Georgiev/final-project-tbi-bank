﻿using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Web.Models.EmailViewModels;
using System;

namespace FinalProjectTBI.Web.Mappers
{
    public class LoanApplicationViewModelMapper : IViewModelMapper<LoanApplication, LoanApplicationViewModel>
    {
        private readonly IViewModelMapper<LoanApplicant, LoanApplicantViewModel> loanApplicantMapper;

        public LoanApplicationViewModelMapper(IViewModelMapper<LoanApplicant, LoanApplicantViewModel> loanApplicantMapper)
        {
            this.loanApplicantMapper = loanApplicantMapper ?? throw new ArgumentNullException(nameof(loanApplicantMapper));
        }

        public LoanApplicationViewModel MapFrom(LoanApplication entity)
        {
            var loanApplicationViewModel = new LoanApplicationViewModel
            {
                Id = entity.Id,
                ModifiedOn = entity.ModifiedOn,
                User = entity.User,
                LoanApplicant = this.loanApplicantMapper.MapFrom(entity.LoanApplicant),
                LoanDecision = entity.LoanDecision,
            };

            return loanApplicationViewModel;
        }
    }
}
