﻿using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Web.Models.EmailViewModels;

namespace FinalProjectTBI.Web.Mappers
{
    public class EmailBodyViewModelMapper : IViewModelMapper<EmailBody, EmailBodyViewModel>
    {
        public EmailBodyViewModel MapFrom(EmailBody entity)
        {
            var emailBodyViewModel = new EmailBodyViewModel
            {
                Id = entity.Id,
                Content = entity.Content,
                ReceivedEmail = entity.ReceivedEmail
            };

            return emailBodyViewModel;
        }
    }
}
