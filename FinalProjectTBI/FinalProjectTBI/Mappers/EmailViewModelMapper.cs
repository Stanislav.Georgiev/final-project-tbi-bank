﻿using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Web.Models.EmailViewModels;
using System;

namespace FinalProjectTBI.Web.Mappers
{
    public class EmailViewModelMapper : IViewModelMapper<ReceivedEmail, EmailViewModel>
    {
        public EmailViewModel MapFrom(ReceivedEmail entity)
        {
            var emailViewModel = new EmailViewModel
            {
                Id = entity.Id,
                Subject = entity.Subject,
                CreatedOn = entity.CreatedOn,
                EmailBodyContent = entity.EmailBody.Content,
                MediatorEmail = entity.Mediator.PersonalEmail,
                EmailStatusType = entity.EmailStatus.Type,
                EmailAttachmentsCount = entity.EmailAttachments.Count,
                Attachments = entity.EmailAttachments,
                UserName = entity.User?.UserName,
                TimeInCurrentStatus = DateTime.Now.Subtract(entity.ModifiedOn).ToString(@"dd\.hh\:mm\:ss")
            };

            return emailViewModel;
        }
    }
}
