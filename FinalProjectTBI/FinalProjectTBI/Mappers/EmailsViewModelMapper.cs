﻿using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Web.Models.EmailViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FinalProjectTBI.Web.Mappers
{
    public class EmailsViewModelMapper : IViewModelMapper<IReadOnlyCollection<ReceivedEmail>, EmailsViewModel>
    {
        private readonly IViewModelMapper<ReceivedEmail, EmailViewModel> receivedEmailMapper;

        public EmailsViewModelMapper(IViewModelMapper<ReceivedEmail, EmailViewModel> receivedEmailMapper)
        {
            this.receivedEmailMapper = receivedEmailMapper ?? throw new ArgumentNullException(nameof(receivedEmailMapper));
        }

        public EmailsViewModel MapFrom(IReadOnlyCollection<ReceivedEmail> emails)
        {
            var emailsViewModel = new EmailsViewModel
            {
                Emails = emails.Select(this.receivedEmailMapper.MapFrom).ToList()
            };

            return emailsViewModel;
        }
    }
}
