﻿using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Web.Models.EmailViewModels;

namespace FinalProjectTBI.Web.Mappers
{
    public class EmailAttachmentViewModelMapper : IViewModelMapper<EmailAttachment, EmailAttachmentViewModel>
    {
        public EmailAttachmentViewModel MapFrom(EmailAttachment entity)
        {
            var emailattachmentViewModel = new EmailAttachmentViewModel
            {
                Id = entity.Id,
                Size = entity.Size,
                FileName = entity.FileName,
                ReceivedEmail = entity.Email
            };
            return emailattachmentViewModel;
        }
    }
}
