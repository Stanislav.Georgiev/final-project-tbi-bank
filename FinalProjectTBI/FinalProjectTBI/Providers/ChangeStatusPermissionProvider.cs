﻿using System.Collections.Generic;

namespace FinalProjectTBI.Web.Providers
{
    public static class ChangeStatusPermissionProvider
    {
        private static readonly Dictionary<(string, string), IEnumerable<string>> ChangeStatusPermission = new Dictionary<(string, string), IEnumerable<string>>
        {
            [("NotReviewed", "Operator")] = new List<string> { "New", "NotValid" },
            [("New", "Operator")] = new List<string> { "Open" },
            [("Open", "Operator")] = new List<string> { "Closed" },
            [("NotValid", "Manager")] = new List<string> { "Not Reviewed" },
            [("NotReviewed", "Manager")] = new List<string> { "New", "NotValid" },
            [("New", "Manager")] = new List<string> { "NotReviewed", "Open" },
            [("Open", "Manager")] = new List<string> { "New", "Closed" },
            [("Closed", "Manager")] = new List<string> { "New" },
        };

        public static IEnumerable<string> GetChangeStatusPermissions(string currentEmailStatus, string userRole)
        {
            return ChangeStatusPermission[(currentEmailStatus, userRole)];
        }
    }
}
