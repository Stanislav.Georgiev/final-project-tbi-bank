﻿using FinalProjectTBI.Data.Models;
using FinalProjectTBI.Web.Models.EmailViewModels;
using System.Collections.Generic;

namespace FinalProjectTBI.Web.Models.HomeViewModels
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {
            LatestEmails = new List<EmailViewModel>();
        }

        public IReadOnlyCollection<EmailViewModel> LatestEmails { get; set; }
    }
}
