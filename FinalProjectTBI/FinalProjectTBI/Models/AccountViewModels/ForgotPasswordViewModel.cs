﻿using System.ComponentModel.DataAnnotations;

namespace FinalProjectTBI.Web.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
