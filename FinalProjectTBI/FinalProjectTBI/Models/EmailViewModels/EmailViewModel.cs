﻿using FinalProjectTBI.Data.Models;
using System;
using System.Collections.Generic;

namespace FinalProjectTBI.Web.Models.EmailViewModels
{
    public class EmailViewModel
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public DateTime CreatedOn { get; set; }
        public string EmailBodyContent { get; set; }
        public string MediatorEmail { get; set; }
        public string EmailStatusType { get; set; }
        public int EmailAttachmentsCount { get; set; }
        public ICollection<EmailAttachment> Attachments { get; set; }
        public string UserName { get; set; }
        public string TimeInCurrentStatus { get; set; }
    }
}
