﻿using FinalProjectTBI.Data.Models;

namespace FinalProjectTBI.Web.Models.EmailViewModels
{
    public class EmailAttachmentViewModel
    {
        public int Id { get; set; }
        public int? Size { get; set; }
        public string FileName { get; set; }
        public ReceivedEmail ReceivedEmail { get; set; }
    }
}
