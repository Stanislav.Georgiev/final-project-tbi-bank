﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalProjectTBI.Web.Models.EmailViewModels
{
    public class NewLoanApplicationViewModel
    {
        public EmailViewModel Email { get; set; }
        public LoanApplicationViewModel LoanApplication { get; set; }
    }
}
