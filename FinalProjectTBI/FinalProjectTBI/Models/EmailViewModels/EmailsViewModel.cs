﻿using System.Collections.Generic;

namespace FinalProjectTBI.Web.Models.EmailViewModels
{
    public class EmailsViewModel
    {
        public IReadOnlyCollection<EmailViewModel> Emails { get; set; }
        public EmailViewModel Emailview { get; set; }
    }
}
