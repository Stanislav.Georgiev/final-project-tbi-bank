﻿using FinalProjectTBI.Data.Models;

namespace FinalProjectTBI.Web.Models.EmailViewModels
{
    public class EmailBodyViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }

        public ReceivedEmail ReceivedEmail { get; set; }
    }
}
