﻿using FinalProjectTBI.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalProjectTBI.Web.Models.EmailViewModels
{
    public class LoanApplicationViewModel
    {
        public int Id { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public ApplicationUser User { get; set; }

        public LoanApplicantViewModel LoanApplicant { get; set; }

        public LoanDecision LoanDecision { get; set; }

        public EmailViewModel Email { get; set; }
    }
}
